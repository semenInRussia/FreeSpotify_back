class NotFoundAlbumException(Exception):
    message = "Album don't find."


class NotFoundArtistException(Exception):
    message = "Artist don't find."
