class Track:
    def __init__(self, artist_name: str, album_name: str, track_name: str):
        self._artist_name = artist_name
        self._album_name = album_name
        self._track_name = track_name
