beautifulsoup4==4.9.3
bs4==0.0.1
Django==3.1.2
requests==2.24.0
loguru~=0.5.3
